import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http'
import {Observable, throwError} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string = 'https://randomuser.me/api/'
  
  constructor(public http: HttpClient) { }




  read(): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })
    return this.http.get(`${this.url}?results=100`,{headers: headers})
  }







  //dataService: any
  //data: any

  //dataObserver: any

  ///constructor(public http: HttpClient) { }


  /*
  read(data: any = null): Observable<any> {
    var params = ''
    if (data) {
      if (data.pageSize) params += 'pageSize=' + data.pageSize + '&'
      if (data.p) params += 'p=' + data.p + '&'
      if (data.q) params += 'q=' + JSON.stringify(data.q) + '&'
      if (data.sort) params += 'sort=' + data.sort + '&'
    }
    return this.http.get(`${this.urlBase}s?${params}`)
      .catch((error: any) => Observable.throw(error || 'Server error'))
  }

  // incompleto
  // create or update
  write(myData): Observable<any> {
    let body = JSON.stringify(myData)
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    if (myData._id) {
      return this.http
        .put(`${this.urlBase}s/${myData._id}`, body, { headers: headers })
    } else {
      return this.http
        .post(`${this.urlBase}s`, body, { headers: headers })
    }
  }




  getOwnLocal(): Observable<any> {
    return new Observable(observer => {
      this.dataObserver = observer
      //this.dataObserver.next({});
    });
  }

  // https://angular-2-training-book.rangle.io/handout/observables/using_observables.html
  getOwn(): Observable<any> {
    var self = this
    return this.http
      .get(`${this.urlBase}/get-own`)
      .map((res: any) => {
        self.dataObserver.next(res);
        return res
      })
  }

  saveOwn(myData): Observable<any> {
    let body = JSON.stringify(myData)
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' })

    return this.http
      .put(`${this.urlBase}/save-own`, body, { headers: headers })
      .map((res: any) => {
        this.dataObserver.next(res);
        return res
      })

    }
    */









}
