import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {

  @Input() users: any = [];

  @Output() selectRequest = new EventEmitter<any>();

  constructor() { 
    //console.log(this.users)
  }

  ngOnInit() {
    //console.log(this.users)
  }


  select(user) {
    console.log(user)
    this.selectRequest.emit(user);
  }
}
