import { Component, OnInit} from '@angular/core';

import  { UserService } from '../user.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{


  users: any = []
  userSelected: any = null

  constructor(private userService: UserService) {
    console.log('HomePage')
  }

  ngOnInit() { 
    console.log('ngOnInit')
    this.userService.read().subscribe(res => {
      this.users = res.results
      console.log('XXXX', res)
    }, err => {
      console.log('XXXXXXXX AUTH ERR!!!')
    })
  }


  selectUser(user) {
    //console.log(user)
    this.userSelected = user
  }

}
